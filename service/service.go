package service

import (
	"context"
	"log"

	c "github.com/double/comment_service/genproto/comment"
	"github.com/double/comment_service/pkg/logger"
	grpcclient "github.com/double/comment_service/service/grpc_client"
	"github.com/double/comment_service/storage"
	"github.com/jmoiron/sqlx"
)

type CommentService struct {
	storage storage.IStorage
	Logger  logger.Logger
	Client  grpcclient.Clients
}

func NewCommentService(db *sqlx.DB, log logger.Logger, client grpcclient.Clients) *CommentService {
	return &CommentService{
		storage: storage.NewStoragePg(db),
		Logger:  log,
		Client:  client,
	}
}

func (s *CommentService) WriteComment(ctx context.Context, req *c.CommentRequest) (*c.CommentResponse, error) {
	res, err := s.storage.Comment().WriteComment(ctx, req)
	if err != nil {
		log.Println("failed to add comment:", err)
		return &c.CommentResponse{}, err
	}
	return res, nil
}

func (s *CommentService) GetPostComments(ctx context.Context, req *c.IdRequest) (*c.Comments, error){
	res, err := s.storage.Comment().GetPostComments(ctx, req)
	if err != nil {
		log.Println("failed to limit comment: ", err)
		return &c.Comments{}, err
	}
	return res, nil
}

func (s *CommentService) GetAllComments(ctx context.Context, req *c.Limit) (*c.Comments, error){
	res, err := s.storage.Comment().GetAllComments(ctx, req)
	if err != nil {
		log.Println("failed to post for comment:", err)
		return &c.Comments{}, err
	}
	return res, nil
}

func (s *CommentService) DeleteComment(ctx context.Context, req *c.IdRequest) (*c.CommentResponse, error){
	res, err := s.storage.Comment().DeleteComment(ctx, req)
	if err != nil {
		log.Println("failed to post for comment:", err)
		return &c.CommentResponse{}, err
	}
	return res, nil
}