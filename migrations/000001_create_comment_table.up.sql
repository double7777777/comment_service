CREATE TABLE IF NOT EXISTS comment (
    id serial primary key,
    post_id integer,
    textComment text,
    created_at TIME DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIME
);