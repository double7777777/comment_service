FROM golang:1.20.4-alpine
RUN mkdir comment
COPY . /comment
WORKDIR /comment
RUN go mod tidy
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 8002