package postgres

import (
	"context"
	"log"
	"time"
	"github.com/opentracing/opentracing-go"
	c "github.com/double/comment_service/genproto/comment"
)

func (r *CommentRepo) WriteComment(ctx context.Context, comment *c.CommentRequest) (*c.CommentResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "WriteComment")
	defer trace.Finish()
	var res c.CommentResponse

	err := r.db.QueryRow(`
	insert into 
		comments(post_id, user_id, text)
	values
		($1, $2, $3) 
	returning 
		id, post_id, user_id, user_name, description, created_at`, comment.ProductId, comment.UserId, comment.Description).Scan(
		&res.Id,
		&res.PostId,
		&res.UserId,
		&res.UserName,
		&res.Description,
		&res.CreatedAt)

	if err != nil {
		log.Println("failed to create comment: ", err)
		return &c.CommentResponse{}, err
	}
	return &res, nil
}

func (r *CommentRepo) GetPostComments(ctx context.Context, com *c.IdRequest) (*c.Comments, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetPostComments")
	defer trace.Finish()
	var res c.Comments

	rows, err := r.db.Query(`
		select 
			id, post_id, user_id,user_name, description, created_at 
		from 
			comments 
		where 
			post_id = $1 and deleted_at is null`, com.PostId)

	if err != nil {
		log.Println("failed to get comment for post: ", err)
		return &c.Comments{}, nil
	}

	for rows.Next() {
		comment := c.CommentResponse{}

		err = rows.Scan(
			&comment.Id,
			&comment.PostId,
			&comment.UserId,
			&comment.UserName,
			&comment.Description,
			&comment.CreatedAt,
		)

		if err != nil {
			log.Println("failed to scanning comment: ", err)
			return &c.Comments{}, err
		}

		res.Comments = append(res.Comments, &comment)
	}
	return &res, nil
}

func (r *CommentRepo) GetAllComments(ctx context.Context, com *c.Limit) (*c.Comments, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "GetAllComments")
	defer trace.Finish()
	var res c.Comments

	rows, err := r.db.Query(`
		select 
			id, post_id, user_id, user_name, description, created_at 
		from 
			comments 
		where 
			deleted_at is null limit $1`, com.Limit)

	if err != nil {
		log.Println("failed to get comment: ", err)
		return &c.Comments{}, nil
	}

	for rows.Next() {
		comment := c.CommentResponse{}

		err = rows.Scan(
			&comment.Id,
			&comment.PostId,
			&comment.UserId,
			&comment.UserName,
			&comment.Description,
			&comment.CreatedAt,
		)

		if err != nil {
			log.Println("failed to scanning comment: ", err)
			return &c.Comments{}, err
		}

		res.Comments = append(res.Comments, &comment)
	}
	return &res, nil
}

func (r *CommentRepo) DeleteComment(ctx context.Context, id *c.IdRequest) (*c.CommentResponse, error) {
	trace, ctx := opentracing.StartSpanFromContext(ctx, "DeleteComment")
	defer trace.Finish()
	var res c.CommentResponse

	err := r.db.QueryRow(`
	UPDATE
		comments 
	SET 
		deleted_at = $1 
	WHERE 
		id = $2 
	RETURNING
		id, post_id, user_id, user_name, description, created_at`, time.Now(), id.PostId).Scan(
		&res.Id,
		&res.PostId,
		&res.UserId,
		&res.UserName,
		&res.Description,
		&res.CreatedAt)

	if err != nil {
		log.Println("failed to delete comment", err)
	}
	return &res, nil
}
