package postgres

import (
	"context"
	"testing"

	"github.com/double/comment_service/config"
	pc "github.com/double/comment_service/genproto/comment"
	"github.com/double/comment_service/pkg/db"
	"github.com/double/comment_service/storage/repo"
	"github.com/stretchr/testify/suite"
)

type CommentSuiteTest struct {
	suite.Suite
	CleanUpFunc func()
	Repo        repo.CommentStorageI
}

func (s *CommentSuiteTest) SetupSuite() {
	pgPool, cleanUp := db.ConnectToDBForSuite(config.Load())
	s.Repo = NewCommentRepo(pgPool)
	s.CleanUpFunc = cleanUp
}

func (s *CommentSuiteTest) TestCommentCrud() {
	comment := &pc.CommentRequest{
		ProductId:   3,
		UserId:      2,
		Description: "commentTest",
	}

	writeComResp, err := s.Repo.WriteComment(context.Background(), comment)
	s.Nil(err)
	s.NotNil(writeComResp)
	s.Equal(comment.ProductId, writeComResp.PostId)
	s.Equal(comment.UserId, writeComResp.UserId)
	s.Equal(comment.Description, writeComResp.Description)

	getPostComments, err := s.Repo.GetPostComments(context.Background(), &pc.IdRequest{PostId: writeComResp.PostId})
	s.Nil(err)
	s.NotNil(getPostComments)
	s.Equal(comment.ProductId, writeComResp.PostId)

	getAllComments, err := s.Repo.GetAllComments(context.Background(), &pc.Limit{Limit: 5})
	s.Nil(err)
	s.NotNil(getAllComments)

	deleteComentResp, err := s.Repo.DeleteComment(context.Background(), &pc.IdRequest{PostId: writeComResp.Id})
	s.Nil(err)
	s.NotNil(deleteComentResp)
}

func (suite *CommentSuiteTest) TearDownSuite() {
	suite.CleanUpFunc()
}

func TestCommentRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(CommentSuiteTest))
}
