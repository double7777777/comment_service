package repo

import (
    "context"
	c "github.com/double/comment_service/genproto/comment"
)

type CommentStorageI interface {
	WriteComment(context.Context, *c.CommentRequest) (*c.CommentResponse, error) 
    GetPostComments(context.Context, *c.IdRequest) (*c.Comments, error) 
    GetAllComments(context.Context, *c.Limit) (*c.Comments, error)
    DeleteComment(context.Context, *c.IdRequest) (*c.CommentResponse, error) 
}
