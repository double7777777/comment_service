package main

import (
	"fmt"
	"net"

	"github.com/double/comment_service/config"
	c "github.com/double/comment_service/genproto/comment"
	"github.com/double/comment_service/pkg/db"
	"github.com/double/comment_service/pkg/logger"
	"github.com/double/comment_service/service"
	grpcclient "github.com/double/comment_service/service/grpc_client"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
)

func main() {
	conf := jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			Type: jaeger.SamplerTypeConst,
			Param: 10,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans: true,
			LocalAgentHostPort: "jaeger:6831",
		},
	}
	closer, err := conf.InitGlobalTracer(
		"comment_service",
	)
	if err != nil{
		fmt.Println(err)
	}
	defer closer.Close()


	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)

	connDb, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error connect postgres", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		fmt.Println("Error connect grpc client: ", err.Error())
	}

	
	commentService := service.NewCommentService(connDb, log, grpcClient)

	commentPort := ":8002"
	lis, err := net.Listen("tcp", commentPort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	c.RegisterCommentServiceServer(s, commentService)

	log.Info("main: server running",
		logger.String("port", cfg.CommentServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
